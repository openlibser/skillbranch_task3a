const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const getPcJson = require('./getPcJson');
const allowCrossDomain = require('./skillBranchCORS');
const generateRoutes = require('./generateRoutes');

const app = express();

app.use(allowCrossDomain);
app.use(morgan('tiny'));

async function preloadPcInfo() {
  const pcInfo = await getPcJson();
  return pcInfo;
}

preloadPcInfo().then((pcInfo) => {
  const filePath = `./jsons_bak/pc_${Math.random().toString().slice(3, 8)}.json`;
  fs.writeFileSync(filePath, JSON.stringify(pcInfo, '', 2));

  app.get('/', (req, res) => {
    res.json(pcInfo);
  });

  app.get('/volumes', (req, res) => {
    const volumeSizes = pcInfo.hdd.reduce((sizes, vol) => {
      const letter = vol.volume;
      if (sizes[letter] === undefined) sizes[letter] = 0;
      sizes[letter] += +vol.size;
      return sizes;
    }, {});

    for (const letter of Object.keys(volumeSizes)) {
      volumeSizes[letter] = `${volumeSizes[letter]}B`;
    }

    res.json(volumeSizes);
  });

  const generatedRoutes = generateRoutes(pcInfo);
  console.log(generatedRoutes);
  const ERROR_MESSAGE_404 = 'Not Found';

  for (const route of Object.keys(generatedRoutes)) {
    app.get(route, generatedRoutes[route]);
  }

  app.get('*', (req, res) => {
    res.status(404).send(ERROR_MESSAGE_404);
  });

  // routes error handling
  app.use((err, req, res, next) => {
    if (err && err.message === '404') {
      res.status(404).send(ERROR_MESSAGE_404);
    } else if (err) {
      res.status(500).send('Server error.');
      console.error(err);
    }
    next();
  });

  app.listen(3000, () => console.log('Server runing on 3000 port!'));
})
.catch(console.error.bind(console));
