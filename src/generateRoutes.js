const genObjectRoute = (obj, propName) => (req, res) => {
  const prop = req.params[propName];
  if (!obj.hasOwnProperty(prop)) throw new Error('404');
  res.json(obj[prop]);
};

const genArrayRoute = (arr, indexPropName) => (req, res) => {
  const index = req.params[indexPropName];
  const isIndexValid = !isNaN(+index) && index < arr.length - 1 && index >= 0;
  if (!isIndexValid) throw new Error('404');
  if (arr[index] === undefined) throw new Error('404');

  res.json(arr[index]);
};

const isThisObject = (obj) => {
  return obj === Object(obj) &&
    Object.prototype.toString.call(obj) === '[object Object]';
};

const getObjectsAndArraysProps = (obj) => {
  return Object.keys(obj).reduce((objsAndArrs, propName) => {
    const isArray = Array.isArray(obj[propName]);
    const isObject = isThisObject(obj[propName]);
    if (isArray || isObject) {
      return [...objsAndArrs, propName];
    }
    return [...objsAndArrs];
  }, []);
};

const genRoutes = (pcInfo) => {
  const routes = {};
  const base = '/';

  const routesGenerator = (infoObj, baseRoutePath) => {
    const isArray = Array.isArray(infoObj);
    const isObject = isThisObject(infoObj);

    if (isObject) {
      const routePath = baseRoutePath === '/'
        ? '/:prop'
        : `${baseRoutePath}/:prop`;
      routes[routePath] = genObjectRoute(infoObj, 'prop');
    }

    if (isArray) {
      const routePath = baseRoutePath === '/'
        ? '/:index'
        : `${baseRoutePath}/:index`;
      routes[routePath] = genArrayRoute(infoObj, 'index');
    }

    const objAndArrs = getObjectsAndArraysProps(infoObj);
    if (!objAndArrs.length) return;

    objAndArrs.forEach((propName) => {
      const nextPath = baseRoutePath === '/'
        ? `/${propName}`
        : `${baseRoutePath}/${propName}`;
      routesGenerator(infoObj[propName], nextPath);
    });
  };

  routesGenerator(pcInfo, base);

  return routes;
};

module.exports = genRoutes;
